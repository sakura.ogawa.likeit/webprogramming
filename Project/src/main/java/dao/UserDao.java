package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 * ユーザテーブル用のDao
 *
 * @author takano
 */
public class UserDao {

  /**
   * upda ログインIDとパスワードに紐づくユーザ情報を返す S
   * 
   * @param loginId
   * @param password
   * @return
   */
  public User findByLoginInfo(String loginId, String password) { // LoginServletから引数をもらう
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection(); // DBManagerでコネクション

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?"; // 検索文用意飛んできたもので調べるため?を使う

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id"); // IDの情報を取得
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  /**
   * 全てのユーザ情報を取得する
   *
   * @return
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM user WHERE is_admin = false";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  public void insert(String loginId, String password, String name, String birthDate) { // LoginServletから引数をもらう
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection(); // DBManagerでコネクション

      // INSERT文を準備
      String sql =
          "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)VALUES(?,?,?,?,now(),now())"; // 検索文用意飛んできたもので調べるため?を使う

      // INSERTを実行し、実行出来た数を表示
      // PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setString(3, birthDate);
      pStmt.setString(4, password);
      int result = pStmt.executeUpdate();

      System.out.println(result);
      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public User findByld(int userId) {
    Connection conn = null;
    PreparedStatement pStmt = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE id = ?";

      // SELECTを実行し、結果表を取得
      // PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, userId);
      ResultSet rs = pStmt.executeQuery();


      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id"); // IDの情報を取得
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public void update(int userId, String name, String birthDate) { // UserUpdateServletから引数をもらう
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection(); // DBManagerでコネクション

      // UPDATE文を準備
      String sql = "UPDATE user SET name = ?, birth_date = ? WHERE id = ?"; // 更新文用意、飛んできたもので更新するため?を使う
      // UPDATEを実行し、実行出来た数を表示
      // PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, birthDate);
      pStmt.setInt(3, userId);
      int result = pStmt.executeUpdate();

      System.out.println(result);
      pStmt.close();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void update(int userId, String password, String name, String birthDate) { // UserUpdateServletから引数をもらう
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection(); // DBManagerでコネクション

      // UPDATE文を準備
      String sql = "UPDATE user SET password = ?, name = ?, birth_date = ? WHERE id = ?"; // 更新文用意、飛んできたもので更新するため?を使う

      // UPDATEを実行し、実行出来た数を表示
      // PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, password);
      pStmt.setString(2, name);
      pStmt.setString(3, birthDate);
      pStmt.setInt(4, userId);
      int result = pStmt.executeUpdate();

      System.out.println(result);
      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public boolean existUser(String loginId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      if (rs.next()) {
        return true;
      }

      return false;

    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return false;
        }
      }
    }
  }


  public void delete(int userId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection(); // DBManagerでコネクション

      // DELETE文を準備
      String sql = "DELETE FROM user WHERE id = ?"; // 検索文用意飛んできたもので調べるため?を使う
      // UPDATEを実行し、実行出来た数を表示
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, userId);
      int result = pStmt.executeUpdate();

      System.out.println(result);
      pStmt.close();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public List<User> search(String loginId, String userName, String startDate, String endDate) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      StringBuilder sql = new StringBuilder("SELECT * FROM user WHERE is_admin = false");
      // パラメータの設定用のリストを作成
      List<String> params = new ArrayList<>();

      if (!(loginId.equals(""))) {
        sql.append(" and login_id = ?");
        params.add(loginId);
      }
      if (!(userName.equals(""))) {
        sql.append(" and name Like ?");
        params.add("%" + userName + "%");
      }
      if (!(startDate.equals(""))) {
        sql.append(" and birth_date >= ?");
        params.add(startDate);
      }
      if (!(endDate.equals(""))) {
        sql.append(" and birth_date <= ?");
        params.add(endDate);
      }


      PreparedStatement pStmt = conn.prepareStatement(sql.toString() + " ORDER BY id ASC");

      for (int i = 0; i < params.size(); i++) {
        pStmt.setObject(i + 1, params.get(i));
      }
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String login_Id = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, login_Id, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }
}
