package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // TODOログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("userInfo");
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      // idを取得し、型変換する
//      String i = request.getParameter("id"); // userListから送られてきたuserDaoのfindAllのid
//      int id = Integer.valueOf(i).intValue();
      int id = Integer.valueOf(request.getParameter("id"));

      // userDaoのfindByldメソッドで詳細情報を手に入れる
      UserDao userDao = new UserDao();
      User userData = userDao.findByld(id);

      request.setAttribute("user", userData);

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp"); // userDelete.jspへuserDateをとばす
      dispatcher.forward(request, response);
      return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
//      request.setCharacterEncoding("UTF-8");
//      if (request.getParameter("button").equals("no")) {
//        // UserListServletにリダイレクト
//        response.sendRedirect("UserListServlet");
//
//      } else if (request.getParameter("button").equals("yes")) {

        // userDelete.jspで送られてきたidを取得し、型変換する
        String i = request.getParameter("id");
        int id = Integer.valueOf(i);

        // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
        UserDao userDao = new UserDao();
        userDao.delete(id); // UserDaoのインスタンスを作り、deleteメソッドに引数を飛ばす


        // UserListServletにリダイレクト
        response.sendRedirect("UserListServlet");
        return;
        // }
	}

  }
