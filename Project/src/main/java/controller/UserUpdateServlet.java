package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // TODOログインセッションがない場合、ログイン画面にリダイレクトさせる
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("userInfo");
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    // idを取得し、型変換する
    // String i = request.getParameter("id"); // userListから送られてきたuserDaoのfindAllのid
    // int id = Integer.valueOf(i).intValue();
    int id = Integer.valueOf(request.getParameter("id"));


    // userDaoのfindByldメソッドで詳細情報を手に入れる
    UserDao userDao = new UserDao();
    User userData = userDao.findByld(id);

    request.setAttribute("user", userData);

    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp"); // userUpdate.jspへuserDateをとばす
    dispatcher.forward(request, response);
    return;

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    String userId = request.getParameter("user-id"); // userUpdate.jspから飛んできた
    String loginId = request.getParameter("login_id");
    String password = request.getParameter("password");
    String cPassword = request.getParameter("password-confirm");
    String name = request.getParameter("user-name");
    String birthDate = request.getParameter("birth-date");

    /** パスワードが一致していない、情報が空欄 * */
    if (!(password.equals(cPassword)) || name.equals("") || birthDate.equals("")) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "入力された内容は正しくありません");

      // ログインIDと入力した名前と生年月日を画面に表示するため、リクエストに値をセット
      request.setAttribute("userId", userId);
      request.setAttribute("loginId", loginId);
      request.setAttribute("name", name);
      request.setAttribute("birthDate", birthDate);

      // userUpdate.jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
      return;

    } else if (password.equals("") && cPassword.equals("")) {
      /** パスワードが一致していて空欄、情報が全て入力されている * */
      // リクエストパラメータの入力項目を取得、idを型変換
      int id = Integer.valueOf(userId);
      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      UserDao userDao = new UserDao();
      userDao.update(id, name, birthDate);// UserDaoのインスタンスを作り、updateメソッドに引数を飛ばす


    } else {
      /** パスワードが一致している、情報が全て入力されている * */
      // パスワードの暗号化
      PasswordEncorder passwordEncorder = new PasswordEncorder();
      String Epassword = passwordEncorder.encordPassword(password);

      // リクエストパラメータの入力項目を取得、idを型変換
      int id = Integer.valueOf(userId);
      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      UserDao userDao = new UserDao();
      userDao.update(id, Epassword, name, birthDate); // UserDaoのインスタンスを作り、updateメソッドに引数を飛ばす

    }

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");
  }

}
