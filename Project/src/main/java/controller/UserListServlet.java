
package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // TODOログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("userInfo");
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
      // ユーザー一覧情報を取得
      UserDao userDao = new UserDao();
      List<User> userList = userDao.findAll();

      // リクエストスコープにユーザー一覧情報をセット
      request.setAttribute("userList", userList);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  //TODO 未実装:検索処理全般
	  // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      String loginId = request.getParameter("login-id"); 
      String name = request.getParameter("user-name");
      String startDate = request.getParameter("start-date");
      String endDate = request.getParameter("end-date");
      
      // userDaoのsearchメソッドで詳細情報を手に入れる
      UserDao userDao = new UserDao();
      List<User> userData = userDao.search(loginId, name, startDate, endDate);

      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("userData", userData);
      request.setAttribute("loginId",loginId);
      request.setAttribute("userName",name);
      request.setAttribute("startDate", startDate);
      request.setAttribute("endDate",endDate);
      
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
}
}
