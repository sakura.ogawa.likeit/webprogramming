package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // TODOログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("userInfo");
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      UserDao userDao = new UserDao();
      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("user-loginid"); // userAdd.jspから飛んできた
      String password = request.getParameter("password");
      String cPassword = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");

      /** パスワードが一致しない、入力していない項目がある場合 * */
      // if (!(password.equals(cPassword)) || password.equals("") || cPassword.equals("") ||
      // loginId.equals("") || name.equals("")
      // || birthDate.equals("")) {
      if (!isInputAll(loginId, password, cPassword, name, birthDate)
          || !(password.equals(cPassword)) || userDao.existUser(loginId)) {
        // メソッド名を決める際、戻り値の型がbooleanであれば「is,has,can,exist」から始めるというルールがある

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        // 入力した名前と生年月日を画面に表示するため、リクエストに値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("userName", name);
        request.setAttribute("birthDate", birthDate);

        // userAdd.jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;


      } else {
        /** パスワードが一致、情報が全て入力されている * */
        // パスワードの暗号化
        PasswordEncorder passwordEncorder = new PasswordEncorder();
        String Epassword = passwordEncorder.encordPassword(password);
      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      // UserDao userDao = new UserDao();
      userDao.insert(loginId, Epassword, name, birthDate); // UserDaoのインスタンスを作り、insertメソッドに引数を飛ばす

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");
    }
	}

    private static boolean isInputAll(String loginId, String password, String confirmPassword,
        String name, String birthdate) {
      return !(loginId.equals("") || password.equals("") || confirmPassword.equals("")
          || name.equals("") || birthdate.equals(""));
    }


  }
