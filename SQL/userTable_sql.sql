CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
use usermanagement;
CREATE TABLE user(
id SERIAL primary key AUTO_INCREMENT NOT NULL,
login_id varchar(255) UNIQUE NOT NULL,
name  varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean  NOT NULL default false,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL
);

INSERT INTO user(login_id,name,birth_date,password,is_admin,create_date,update_date) VALUES(
'admin','管理者','2023-03-03','password',true,NOW(),NOW()
);


INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    create_date,
    update_date
)
VALUES(
    'user01',
    '一般1',
    '2001-12-31',
    'password',
    now(),
    now()
);

INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    create_date,
    update_date
)
VALUES(
    'user02',
    '一般2',
    '2001-12-31',
    'password',
    now(),
    now()
);

INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    create_date,
    update_date
)
VALUES(
    'user03',
    '一般3',
    '2001-12-31',
    'password',
    now(),
    now()
);
SELECT * FROM user WHERE login_id !='admin'
